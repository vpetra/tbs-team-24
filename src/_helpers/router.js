import Vue from 'vue';
import Router from 'vue-router';

import ContractorPage from '../contractor/ContractorPage'
import OperatorPage from '../operator/OperatorPage'
import LoginPage from '../login/LoginPage'
import RegisterPage from '../register/RegisterPage'

Vue.use(Router);

export const router = new Router({
  mode: 'history',
  routes: [
    {
      name: 'contractor',
      path: '/contractor',
      component: ContractorPage,
      meta: {
        isContractor: true,
        isOperator: false
      }
    },
    {
      name: 'operator',
      path: '/',
      component: OperatorPage,
      meta: {
        isContractor: false,
        isOperator: true
      }
    },
    {
      name: 'login',
      path: '/login',
      component: LoginPage
    },
    {
      name: 'register',
      path: '/register',
      component: RegisterPage
    }
  ]
});

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/login', '/register'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  if (authRequired && !loggedIn) {
    return next('/login');
  } else if (loggedIn) {
    let userType = JSON.parse(loggedIn).userType;
    console.log(userType);
    if (to.meta.isContractor){
      if (userType === 'Contractor') {
        return next();
      } else {
        return next('/');
      }
    } else if(to.meta.isOperator){
      if (userType === 'Operator') {
        return next();
      } else {
        return next('/contractor');
      }
    }
  }

  next();
})