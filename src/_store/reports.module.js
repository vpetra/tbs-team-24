import { reportService } from '../_services';

const state = {
    all: {},
    contractor: {},
    status: {}
};

const actions = {
    register({ dispatch, commit }, report) {
        commit('registerRequest', report);
    
        reportService.addReport(report)
            .then(
                report => {
                    commit('registerSuccess', report);
                    setTimeout(() => {
                        // display success message after route change completes
                        dispatch('alert/success', 'Report added successful', { root: true });
                    })
                },
                error => {
                    commit('registerFailure', error);
                    dispatch('alert/error', error, { root: true });
                }
            );
    },

    getAll({ commit }) {
        commit('getAllRequest');

        reportService.getAllReports()
            .then(
                reports => commit('getAllSuccess', reports),
                error => commit('getAllFailure', error)
            );
    }
};

const mutations = {
    registerRequest(state, report) {
        state.status = { registering: true };
    },
    registerSuccess(state, report) {
        state.status = {};
    },
    registerFailure(state, error) {
        state.status = {};
    },
    getAllRequest(state) {
        state.all = { loading: true };
    },
    getAllSuccess(state, reports) {
        state.all = { items: reports };
    },
    getAllFailure(state, error) {
        state.all = { error };
    }
};

export const reports = {
    namespaced: true,
    state,
    actions,
    mutations
};
